
package org.innovatis.inMigrate.batchProcessor;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import org.innovatis.inMigrate.batchService.JobPropertyMapService;
import org.innovatis.inMigrate.freeMarkerManager.FreemarkerManager;
import org.innovatis.inMigrate.inInterfaces.InProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import freemarker.template.TemplateException;

public class DbFsAlfrescoProcessorImpl implements InProcessor {

	private static final Logger log = LoggerFactory.getLogger(DbFsAlfrescoProcessorImpl.class);
	
	private String bulkMetaDataExtension;
	
	private String bulkMetaDataFtl;
	
	private String folderID;
	
	private String fileName;
	
	private String createdDate;
	
	private Map jobParam;

	public void setJobParam(Map jobParam) {
		this.jobParam = jobParam;
	}

	private void setProperties(String jobName) {
		this.bulkMetaDataExtension=JobPropertyMapService.getJobProperty(jobName,"org.innovatis.inMigration.bulkMetaDataExtension");
		this.folderID=JobPropertyMapService.getJobProperty(jobName,"org.innovatis.inMigration.customObjectMap.folderID");
		this.fileName=JobPropertyMapService.getJobProperty(jobName,"org.innovatis.inMigration.customObjectMap.fileName");
		this.createdDate=JobPropertyMapService.getJobProperty(jobName,"org.innovatis.inMigration.customObjectMap.createdDate");
		this.bulkMetaDataFtl=JobPropertyMapService.getJobProperty(jobName,"org.innovatis.inMigration.bulkMetaDataFtl");
	}
	
	@Override
	public Map<String,String> process(final Map<String,String> bulkMetadata) throws IOException, TemplateException{
		setProperties((String)jobParam.get("jobName"));
		File file1 = new File((String)jobParam.get("bulkDataPath") + (String) bulkMetadata.get(folderID));
		File file2 = new File((String)jobParam.get("bulkDataPath") + (String) bulkMetadata.get(folderID)+bulkMetaDataExtension);
		if(!file1.exists()){
			file1.mkdir();
		}
		if(!file2.exists()){
			file2.createNewFile();
		}
		Map<String, String> map = new HashMap<String, String>();
		FileWriter writer = new FileWriter(file2);
		map.put(fileName, bulkMetadata.get(folderID));
		map.put(createdDate, LocalDateTime.now().toString());
		map.put("type", "folder");
		FreemarkerManager.get().process(bulkMetaDataFtl, map, writer);
		writer.close();
		return bulkMetadata;
	}

}