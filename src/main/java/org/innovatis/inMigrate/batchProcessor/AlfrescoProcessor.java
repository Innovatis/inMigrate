
package org.innovatis.inMigrate.batchProcessor;

import java.io.File;
import java.util.Map;

import org.innovatis.inMigrate.batchService.CSVDataStore;
import org.innovatis.inMigrate.batchService.CountFolderCreation;
import org.innovatis.inMigrate.batchService.JobPropertyMapService;
import org.innovatis.inMigrate.inInterfaces.InProcessor;
import org.innovatis.inMigrate.transformer.AlfrescoDataTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class AlfrescoProcessor implements InProcessor  {

	private Map jobParam;

	@Autowired
	@Qualifier("alfrescoDataTransformerImpl")
	AlfrescoDataTransformer alfrescoDataTransformerImpl; 
	
	public void setJobParam(Map jobParam) {
		this.jobParam = jobParam;
	}
	
	@Autowired
	CSVDataStore csvDataStore;
	
	@Autowired
	CountFolderCreation countFolderCreation;
	
	@Override
	public Map<String, String> process(Map<String, String> item) throws Exception{
		
		if(item.get("content_url")!=null){
			String temp = item.get("content_url").replace("store://", "");
			item.put("content_url",temp.replace("/", File.separator));
		}
		item=alfrescoDataTransformerImpl.transform(item);
		item = countFolderCreation.createFolder(item, (String)jobParam.get("bulkDataPath"),(String)jobParam.get("jobName"), JobPropertyMapService.getJobProperty((String)jobParam.get("jobName"),"org.innovatis.inMigration.alfrescoFtl"));
		return item;
	}

}
