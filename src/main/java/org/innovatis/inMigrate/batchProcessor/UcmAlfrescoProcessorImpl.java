package org.innovatis.inMigrate.batchProcessor;

import java.util.Map;

import org.innovatis.inMigrate.batchService.BulkItemProcessorHelper;
import org.innovatis.inMigrate.batchService.CSVDataStore;
import org.innovatis.inMigrate.batchService.HierarchyFolderCreation;
import org.innovatis.inMigrate.batchService.JobPropertyMapService;
import org.innovatis.inMigrate.inInterfaces.InProcessor;
import org.springframework.beans.factory.annotation.Autowired;

public class UcmAlfrescoProcessorImpl implements InProcessor  {

	private Map jobParam;

	public void setJobParam(Map jobParam) {
		this.jobParam = jobParam;
	}
	@Autowired
	BulkItemProcessorHelper bulkItemProcessorHelper;
	
	@Autowired
	CSVDataStore csvDataStore;

	private String CSV_KEY;
	
	@Autowired
	HierarchyFolderCreation countFolderCreation;
	
	private boolean processMetadata;
	
	private String requiredMetadata;
	
	public void setProperties(Map jobParam){
		this.requiredMetadata=JobPropertyMapService.getJobProperty((String)jobParam.get("jobName"),"org.innovatis.inMigration.process.requiredMetadata");
		this.processMetadata=Boolean.parseBoolean(JobPropertyMapService.getJobProperty((String)jobParam.get("jobName"),"org.innovatis.inMigration.processMetadata"));
		this.CSV_KEY=JobPropertyMapService.getJobProperty((String)jobParam.get("jobName"),"org.innovatis.inMigration.customArciveObjectMap.CSVDataStoreKey");
	}
	
	@Override
	public Map<String, String> process(Map<String, String> item) throws Exception{
		setProperties(jobParam);
		Map<String, String> processedMetadata = bulkItemProcessorHelper.processMetadata(item, processMetadata, requiredMetadata);
		for (Map.Entry<String, String> entry : processedMetadata.entrySet())
		{
			String CSVMapItem = csvDataStore.getCSVMapItem(processedMetadata.get(CSV_KEY), entry.getKey());
			if(!CSVMapItem.equals(null)&&!CSVMapItem.equals(""))
				processedMetadata.put(entry.getKey(), CSVMapItem);
		}
		processedMetadata = countFolderCreation.createFolder(processedMetadata, (String)jobParam.get("bulkDataPath"),(String)jobParam.get("jobName"),JobPropertyMapService.getJobProperty((String)jobParam.get("jobName"),"org.innovatis.inMigration.bulkArchiveMetaDataFtl"));
		return processedMetadata;
	}

}
