package org.innovatis.inMigrate.batchReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.innovatis.inMigrate.batchService.JobPropertyMapService;
import org.innovatis.inMigrate.inInterfaces.InReader;
import org.springframework.batch.item.ReaderNotOpenException;
import org.springframework.batch.item.file.BufferedReaderFactory;
import org.springframework.batch.item.file.DefaultBufferedReaderFactory;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.NonTransientFlatFileException;
import org.springframework.batch.item.file.ResourceAwareItemReaderItemStream;
import org.springframework.batch.item.support.AbstractItemCountingItemStreamItemReader;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;

public class HdaUcmReader extends AbstractItemCountingItemStreamItemReader<Map<String,String>> implements
ResourceAwareItemReaderItemStream<Map<String,String>>, InitializingBean, InReader{
	private String encoding;
	private Map jobParam;

	public void setJobParam(Map jobParam) {
		this.jobParam = jobParam;
		}
	private static final Log logger = LogFactory.getLog(HdaUcmReader.class);
	
	private boolean noInput = false;
	private boolean strict = true;
	private Integer lineCount=0;
	private Integer Count = 0;
	private Integer coloumnCount = 0;
	private List<String> keyList;
	private Resource resource;
	private BufferedReader reader;
	private BufferedReaderFactory bufferedReaderFactory = new DefaultBufferedReaderFactory();

	public HdaUcmReader() {
		setName(ClassUtils.getShortName(FlatFileItemReader.class));
	}
	
	public void setProperties(Map jobParam){
		this.encoding=JobPropertyMapService.getJobProperty((String)jobParam.get("jobName"),"org.innovatis.inMigration.archiveExport.encoding");
		setResource(new FileSystemResource(JobPropertyMapService.getJobProperty((String)jobParam.get("jobName"),"org.innovatis.inMigration.customArciveObjectMap.exportDataPath")+JobPropertyMapService.getJobProperty((String)jobParam.get("jobName"),"org.innovatis.inMigration.customArciveObjectMap.metadataFilePath")));
		}
	
	@Override
	public void afterPropertiesSet() throws Exception {
	}
	
	@Override
	public void setResource(Resource resource) {
		this.resource = resource;
		
	}
	
	private Map<String,String> readItem() {
		if (reader == null) {
			throw new ReaderNotOpenException("Reader must be open before it can be read.");
		}
		Map<String,String> itemMap = new LinkedHashMap<>();
		List<String> itemKeyList = new LinkedList<>();
		String line = null;

		try {
			if(keyList==null){
				line = this.reader.readLine();
				lineCount++;
				while(!line.startsWith("@ResultSet")){
					line = reader.readLine();/*
					if (line == null) {
						return itemMap;
					}*/
					lineCount++;
				}
				line = reader.readLine();
				//to remove d and trim the string after space
				if(line.contains(" ")){
					line = line.substring(0, line.indexOf(" "));
				}
				lineCount++;
				this.coloumnCount = Integer.parseInt(line);
				for(Integer i=0; i<coloumnCount; i++){
					line = reader.readLine();
					//to remove d and trim the string after space
					if(line.contains(" ")){
						line = line.substring(0, line.indexOf(" "));
					}
					if (line.startsWith("@end")) {
						return null;
					}
					itemKeyList.add(line);
					lineCount++;
				}
				keyList = itemKeyList;
			}
			
			for(String key : keyList){
				line = reader.readLine();
				if (line.startsWith("@end")) {
					return null;
				}
				itemMap.put(key,line);
				lineCount++;
			}
			return itemMap;
		}
		catch (IOException e) {
			// Prevent IOException from recurring indefinitely
			// if client keeps catching and re-calling
			noInput = true;
			throw new NonTransientFlatFileException("Unable to read from resource: [" + resource + "]", e, line,
					lineCount);
		}
	}
	@Override
	protected Map<String,String> doRead() throws Exception {
		if (noInput) {
			return null;
		}
		else {
			try {
				return readItem();
			}
			catch (Exception ex) {
				throw new Exception("Parsing error at line: " + lineCount + " in resource=["
						+ resource.getDescription() + "]");
			}
		}
	}
	@Override
	protected void doClose() throws Exception {
		lineCount=0;
		Count = 0;
		coloumnCount = 0;
		keyList = null;
		if (reader != null) {
			reader.close();
		}
	}

	@Override
	protected void doOpen() throws Exception {
		setProperties(jobParam);
		Assert.notNull(resource, "Input resource must be set");

		noInput = true;
		if (!resource.exists()) {
			if (strict) {
				throw new IllegalStateException("Input resource must exist (reader is in 'strict' mode): " + resource);
			}
			logger.warn("Input resource does not exist " + resource.getDescription());
			return;
		}

		if (!resource.isReadable()) {
			if (strict) {
				throw new IllegalStateException("Input resource must be readable (reader is in 'strict' mode): "
						+ resource);
			}
			logger.warn("Input resource is not readable " + resource.getDescription());
			return;
		}

		reader = bufferedReaderFactory.create(resource, encoding);
		noInput = false;
	}


}
