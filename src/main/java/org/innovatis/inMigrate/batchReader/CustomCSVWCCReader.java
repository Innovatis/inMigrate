package org.innovatis.inMigrate.batchReader;

import java.io.BufferedReader;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.item.ReaderNotOpenException;
import org.springframework.batch.item.file.BufferedReaderFactory;
import org.springframework.batch.item.file.DefaultBufferedReaderFactory;
import org.springframework.batch.item.file.NonTransientFlatFileException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

@Component
public class CustomCSVWCCReader {

	@Value("${org.innovatis.inMigration.archiveExport.encoding}")
	private String encoding;

	private static final Log logger = LogFactory.getLog(HdaUcmReader.class);
	private Resource resource;
	private boolean noInput = false;
	private boolean strict = true;
	private Integer lineCount=0;
	private BufferedReader reader;
	private BufferedReaderFactory bufferedReaderFactory = new DefaultBufferedReaderFactory();
	
	private List<Map<String,String>> readItem() {
		if (reader == null) {
			throw new ReaderNotOpenException("Reader must be open before it can be read.");
		}
		List<Map<String,String>> itemMapList = new LinkedList<>();
		String line = null;

		try {
			line = this.reader.readLine();
			String[]  keyList = line.split(",");
				if (line == null) {
					return null;
				}
				lineCount++;
				keyList = line.split(",");
			line = this.reader.readLine();
			while(line!=null){

				Map<String,String> itemMap = new LinkedHashMap<>();
				lineCount++;
				String[]  valueList = line.split(",");
				for(int i=0; i<keyList.length; i++){
					itemMap.put(keyList[i],valueList[i]);
				}
				itemMapList.add(itemMap);
				line = this.reader.readLine();
			}
			doClose();
			return itemMapList;
		}
		catch (Exception e) {
			// Prevent IOException from recurring indefinitely
			// if client keeps catching and re-calling
			noInput = true;
			throw new NonTransientFlatFileException("Unable to read from resource: [" + resource + "]", e, line,
					lineCount);
		}
	}
	
	public List<Map<String,String>> doRead(String csvPath, String encoding) throws Exception {

		doOpen(csvPath,encoding);
		if (noInput) {
			return null;
		}
		else {
			try {
				return readItem();
			}
			catch (Exception ex) {
				throw new Exception("Parsing error at line: " + lineCount + " in resource=["
						+ resource.getDescription() + "]");
			}
		}
	}
	
	protected void doClose() throws Exception {
		lineCount=0;
		if (reader != null) {
			reader.close();
		}
	}

	protected void doOpen(String csvPath, String encoding) throws Exception {
		resource = new FileSystemResource(csvPath);
		noInput = true;
		if (!resource.exists()) {
			if (strict) {
				throw new IllegalStateException("Input resource must exist (reader is in 'strict' mode): " + resource);
			}
			logger.warn("Input resource does not exist " + resource.getDescription());
			return;
		}

		if (!resource.isReadable()) {
			if (strict) {
				throw new IllegalStateException("Input resource must be readable (reader is in 'strict' mode): "
						+ resource);
			}
			logger.warn("Input resource is not readable " + resource.getDescription());
			return;
		}
		noInput = false;
		reader = bufferedReaderFactory.create(resource, encoding);
	}


}
