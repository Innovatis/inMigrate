package org.innovatis.inMigrate.batchReader;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.innovatis.inMigrate.batchService.JobPropertyMapService;
import org.innovatis.inMigrate.inInterfaces.InReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.jdbc.support.JdbcUtils;

public class AlfrescoReader extends JdbcCursorItemReader<Map<String,String>> implements InReader{
	private Map jobParam;

	public void setJobParam(Map jobParam) {
		this.jobParam = jobParam;
		}
	private Connection dbCon;
	@Override
	protected Map<String,String> readCursor(ResultSet rs, int currentRow) throws SQLException {
		return mapRow(rs,currentRow);
	}

	public Map<String,String> mapRow(ResultSet rs, int rowNumber) throws SQLException {
		Map<String,String> resultSetMap = new HashMap<String, String>();
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnCount = rsmd.getColumnCount();
		for (int index = 1; index <= columnCount; index++) {
			String column = JdbcUtils.lookupColumnName(rsmd, index);
			resultSetMap.put(column, rs.getString(column));
			if(JobPropertyMapService.getJobProperty((String) jobParam.get("jobName"),"org.innovatis.inMigration.alfresco.file.nodeId") .equals(column)){
				ResultSet newRs = dbCon.createStatement().executeQuery(JobPropertyMapService.getJobProperty((String) jobParam.get("jobName"),"org.innovatis.inMigration.alfresco.metadata.sql") + rs.getString(column));
				while(newRs.next()){
					resultSetMap.put(newRs.getString(JobPropertyMapService.getJobProperty((String) jobParam.get("jobName"),"org.innovatis.inMigration.alfresco.metadata.db.key")),newRs.getString(JobPropertyMapService.getJobProperty((String) jobParam.get("jobName"),"org.innovatis.inMigration.alfresco.metadata.db.value")));
				}
			}
		}
		return resultSetMap;
	}
	@Override
	protected void openCursor(Connection con) {
		this.dbCon = con;
		setSql(JobPropertyMapService.getJobProperty((String) jobParam.get("jobName"),"org.innovatis.inMigration.alfresco.file.sql"));
		super.openCursor(con);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
	}

}
