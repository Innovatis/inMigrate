package org.innovatis.inMigrate.batchReader;

import org.innovatis.inMigrate.data.Person;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;

@Component
public class PersonFieldSetMapper implements FieldSetMapper<Person> {

	@Override
	public Person mapFieldSet(FieldSet fieldSet) throws BindException {

		Person person = new Person(fieldSet.readString("firstName"), fieldSet.readString("lastName"));

		return person;
	}

}