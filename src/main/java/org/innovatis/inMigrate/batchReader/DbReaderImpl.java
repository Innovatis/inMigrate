package org.innovatis.inMigrate.batchReader;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.innovatis.inMigrate.batchService.JobPropertyMapService;
import org.innovatis.inMigrate.inInterfaces.InReader;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.jdbc.support.JdbcUtils;

public class DbReaderImpl extends JdbcCursorItemReader<Map<String,String>> implements InReader{
	private Map jobParam;

	public void setJobParam(Map jobParam) {
		this.jobParam = jobParam;
		}
	@Override
	protected Map<String,String> readCursor(ResultSet rs, int currentRow) throws SQLException {
		return mapRow(rs,currentRow);
	}

	public Map<String,String> mapRow(ResultSet rs, int rowNumber) throws SQLException {
		Map<String,String> resultSetMap = new HashMap<String, String>();
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnCount = rsmd.getColumnCount();
		for (int index = 1; index <= columnCount; index++) {
			String column = JdbcUtils.lookupColumnName(rsmd, index);
			resultSetMap.put(column, rs.getString(column));
		}
		return resultSetMap;
	}
	

	@Override
	protected void openCursor(Connection con) {
		setSql(JobPropertyMapService.getJobProperty((String) jobParam.get("jobName"),"org.innovatis.inMigration.customObjectMap.metadataSQL"));
		super.openCursor(con);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
	}
}
