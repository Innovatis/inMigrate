package org.innovatis.inMigrate.data;


import javax.persistence.*;

@Entity(name="People")
@Table(name="People")
public class Person {

	// "customer_seq" is Oracle sequence name.
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PEOPLE_SEQ")
    @SequenceGenerator(sequenceName = "PEOPLE_SEQ", allocationSize = 1, name = "PEOPLE_SEQ")
    Long id;

    private String lastName;
    private String firstName;

	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public Person(String lastName, String firstName) {
		super();
		this.lastName = lastName;
		this.firstName = firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
}