package org.innovatis.inMigrate.data.dao;

import org.innovatis.inMigrate.data.PropertyMap;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface PropertyMapDao extends CrudRepository<PropertyMap, Long> {

    List<PropertyMap> findAll();
    PropertyMap save(PropertyMap propertyMap);
}
