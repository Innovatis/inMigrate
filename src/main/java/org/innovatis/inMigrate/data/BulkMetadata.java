package org.innovatis.inMigrate.data;

import java.time.LocalDateTime;

import javax.persistence.*;

@Entity(name = "BulkFiles")
@Table(name = "BulkFiles")
public class BulkMetadata {

	// "customer_seq" is Oracle sequence name.
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BULKFILES_SEQ")
	@SequenceGenerator(sequenceName = "BULKFILES_SEQ", allocationSize = 1, name = "BULKFILES_SEQ")
	Long id;

	private String name;
	private String path;
	private LocalDateTime createdDate;
	private LocalDateTime modifiedDate;
	private String type;
	private String description;
	private String folderId;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public LocalDateTime getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(LocalDateTime modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFolderId() {
		return folderId;
	}

	public void setFolderId(String folderId) {
		this.folderId = folderId;
	}

}