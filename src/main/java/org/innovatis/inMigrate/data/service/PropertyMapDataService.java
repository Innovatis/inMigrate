package org.innovatis.inMigrate.data.service;

import java.util.List;
import java.util.Optional;

import org.innovatis.inMigrate.data.PropertyMap;
import org.innovatis.inMigrate.data.dao.PropertyMapDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PropertyMapDataService implements PropertyMapDao{
	private final PropertyMapDao propertyMapDao;

	@Autowired
	PropertyMapDataService(PropertyMapDao propertyMapDao) {
		this.propertyMapDao = propertyMapDao;
	}

	@Override
	public <S extends PropertyMap> Iterable<S> saveAll(Iterable<S> entities) {
		return null;
	}

	@Override
	public Optional<PropertyMap> findById(Long id) {
		return null;
	}

	@Override
	public boolean existsById(Long id) {
		return false;
	}

	@Override
	public Iterable<PropertyMap> findAllById(Iterable<Long> ids) {
		return null;
	}

	@Override
	public long count() {
		return 0;
	}

	@Override
	public void deleteById(Long id) {
		
	}

	@Override
	public void delete(PropertyMap entity) {
		
	}

	@Override
	public void deleteAll(Iterable<? extends PropertyMap> entities) {
		
	}

	@Override
	public void deleteAll() {
		
	}

	@Override
	public List<PropertyMap> findAll() {
		return propertyMapDao.findAll();
	}

	@Override
	public PropertyMap save(PropertyMap propertyMap) {
		return propertyMapDao.save(propertyMap);
	}
	
}
