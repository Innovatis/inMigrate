package org.innovatis.inMigrate.data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity(name = "PropertyMap")
@Table(name = "PropertyMap")
public class PropertyMap {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PROPERTYMAP_SEQ")
	@SequenceGenerator(sequenceName = "PROPERTYMAP_SEQ", allocationSize = 1, name = "PROPERTYMAP_SEQ")
	Long id;
	
	private String category;
	private String propertyKey;
	private String propertyValue;

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getPropertyKey() {
		return propertyKey;
	}

	public void setPropertyKey(String propertyKey) {
		this.propertyKey = propertyKey;
	}

	public String getPropertyValue() {
		return propertyValue;
	}

	public void setPropertyValue(String propertyValue) {
		this.propertyValue = propertyValue;
	}
}
