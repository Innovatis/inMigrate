package org.innovatis.inMigrate.batchResource;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;

import org.innovatis.inMigrate.batchService.BulkUploadService;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("job")
public class JobExcecutionResource {

	@Autowired
	JobLauncher jobLauncher;

	@Autowired
	@Qualifier("ucmAlfrescoJob")
	Job job;

	@Autowired
	@Qualifier("dataSource")
	DataSource dataSource;
	
	@Autowired
	BulkUploadService bulkUploadService;
	
	@Value("${org.innovatis.inMigration.bulkArchivedDataPath}")
	private String bulkArchivedDataPath;

	@RequestMapping("/execute")
	public void handle() throws Exception {
		JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis()).addString("bulkArchivePath", bulkArchivedDataPath)
				.toJobParameters();
		jobLauncher.run(job, jobParameters);
	}
/*
	@RequestMapping("/help")
	public void helpHandling() throws Exception {
		bulkUploadService.alfrescoBulkUpload("");

	}
	*/
	
	@RequestMapping("/test")
	public void testHandling() throws Exception {
		File file = new File("C:\\Users\\murali\\Downloads\\MountApp\\Bulk\\test\\1");
		if(!file.exists()){
			file.mkdir();
		}
	}
	
	@RequestMapping("/sql")
	public void qeuryHandling() throws Exception {
		Connection connection = dataSource.getConnection();
		//PreparedStatement preparedStatement = connection.prepareStatement("Insert into BULKFILES(id, NAME, path, createdDate, description, type, modifiedDate, folderId) values(2, '9.jpg', 'C:\\Users\\murali\\Downloads\\1.jpg', CURRENT_TIMESTAMP,'pic','jpg',CURRENT_TIMESTAMP, 1)", ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
		PreparedStatement preparedStatement = connection.prepareStatement("Insert into PropertyMap(id, propertyKey, propertyValue) values(1,'mli','rty')", ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
		preparedStatement.execute();
		preparedStatement.close();
	}

}
