package org.innovatis.inMigrate.transformer.impl;

import java.util.Map;

import org.innovatis.inMigrate.batchService.BulkItemProcessorHelper;
import org.innovatis.inMigrate.transformer.AlfrescoDataTransformer;
import org.springframework.beans.factory.annotation.Autowired;

public class AlfrescoDataTransformerImpl implements AlfrescoDataTransformer {

	@Autowired
	BulkItemProcessorHelper bulkItemProcessorHelper;
	
	@Override
	public Map<String, String> transform(Map<String, String> item) {
		//old key = ignore
		item = bulkItemProcessorHelper.processMetadata(item, false, "");
				
		//old key to new key
		item.put("modifiedDate", item.get("updated"));
		//new key
		item.put("test", "Transform");
		//old key1 + old Key2 = new key1
		if(item.get("owner")!=null&&item.get("name")!=null)
			item.put("totalDescription", item.get("owner").concat(item.get("name")));
		item.remove("updated");
		//old key1 = new key1 + new key 2
		if(item.get("test")!=null&&item.get("totalDescription")!=null)
			item.put("ram",item.get("test").concat(item.get("totalDescription")));
		return item;
	}

}
