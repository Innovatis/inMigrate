package org.innovatis.inMigrate.transformer;

import java.util.Map;

public interface AlfrescoDataTransformer {
	
	public Map<String, String> transform(Map<String, String> item);
	
}
