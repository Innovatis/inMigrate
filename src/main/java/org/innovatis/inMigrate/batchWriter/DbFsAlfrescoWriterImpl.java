package org.innovatis.inMigrate.batchWriter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.List;
import java.util.Map;

import org.innovatis.inMigrate.batchService.JobPropertyMapService;
import org.innovatis.inMigrate.freeMarkerManager.FreemarkerManager;
import org.innovatis.inMigrate.inInterfaces.InWriter;
import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DbFsAlfrescoWriterImpl implements InWriter {

	
	private String folderID;
	
	private String fileName;
	
	private String path;
	
	private String bulkMetaDataExtension;
	
	private String bulkMetaDataFtl;
	

	private Map jobParam;
	private void setProperties(String jobName) {
		this.bulkMetaDataExtension=JobPropertyMapService.getJobProperty(jobName,"org.innovatis.inMigration.bulkMetaDataExtension");
		this.folderID=JobPropertyMapService.getJobProperty(jobName,"org.innovatis.inMigration.customObjectMap.folderID");
		this.fileName=JobPropertyMapService.getJobProperty(jobName,"org.innovatis.inMigration.customObjectMap.fileName");
		this.bulkMetaDataFtl=JobPropertyMapService.getJobProperty(jobName,"org.innovatis.inMigration.bulkMetaDataFtl");
		this.path=JobPropertyMapService.getJobProperty(jobName,"org.innovatis.inMigration.customObjectMap.path");
	}
	
	@Override
	public void write(List<? extends Map<String,String>> items) throws FileNotFoundException, IOException, TemplateException{
		setProperties((String)jobParam.get("jobName"));
		for (Map<String,String> bulkMetadata : items) {
			String fileCreationPath = (String)jobParam.get("bulkDataPath") + bulkMetadata.get(folderID)+"//";

			File file = new File(fileCreationPath + bulkMetadata.get(fileName)
					+ bulkMetaDataExtension);
			if (!file.exists()) {
				file.createNewFile();
				log.debug("File is created %s", file.getAbsolutePath());
			} 

			// Write Content
			FileWriter writer = new FileWriter(file);
			bulkMetadata.put("type", "content");
			FreemarkerManager.get().process(bulkMetaDataFtl, bulkMetadata, writer);
			writer.close();
			File file1 = new File(fileCreationPath + bulkMetadata.get(fileName));
			File file2 = new File(bulkMetadata.get(path));
			copyFile(file2, file1);
		}
	}

	private static void copyFile(File sourceFile, File destFile) throws FileNotFoundException, IOException {
		if (!sourceFile.exists()) {
			throw new FileNotFoundException("no file found" + sourceFile.getPath() +"while preparing data fo bulk file upload");
		}
		if (!destFile.exists()) {
			destFile.createNewFile();
		}
		FileChannel source = null;
		FileChannel destination = null;
		source = new FileInputStream(sourceFile).getChannel();
		destination = new FileOutputStream(destFile).getChannel();
		if (destination != null && source != null) {
			destination.transferFrom(source, 0, source.size());
		}
		if (source != null) {
			source.close();
		}
		if (destination != null) {
			destination.close();
		}

	}

	public void setJobParam(Map jobParam) {
		this.jobParam = jobParam;
	}
}