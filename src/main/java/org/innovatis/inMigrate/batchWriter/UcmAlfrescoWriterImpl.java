package org.innovatis.inMigrate.batchWriter;

import java.io.File;
import java.io.FileWriter;
import java.util.List;
import java.util.Map;

import org.innovatis.inMigrate.batchService.BulkItemWriterHelper;
import org.innovatis.inMigrate.batchService.JobPropertyMapService;
import org.innovatis.inMigrate.freeMarkerManager.FreemarkerManager;
import org.innovatis.inMigrate.inInterfaces.InWriter;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UcmAlfrescoWriterImpl implements InWriter {
	
	private String bulkArchivedDataPath;
	
	private String folderID;
	
	private String fileName;
	
	private String relativePath;
	
	private String bulkMetaDataExtension;

	private String bulkArchiveMetaDataFtl;
	
	private String exportDataPath;
	
	private Map jobParam;

	public void setJobParam(Map jobParam) {
		this.jobParam = jobParam;
	}
	
	@Autowired
	BulkItemWriterHelper bulkItemWriterHelper;

	public void setProperties(Map jobParam){
		this.bulkArchivedDataPath=JobPropertyMapService.getJobProperty((String)jobParam.get("jobName"),"org.innovatis.inMigration.bulkArchivedDataPath");
		this.folderID=JobPropertyMapService.getJobProperty((String)jobParam.get("jobName"),"org.innovatis.inMigration.customArciveObjectMap.folderID");
		this.fileName=JobPropertyMapService.getJobProperty((String)jobParam.get("jobName"),"org.innovatis.inMigration.customArciveObjectMap.fileName");
		this.relativePath=JobPropertyMapService.getJobProperty((String)jobParam.get("jobName"),"org.innovatis.inMigration.customArciveObjectMap.relativePath");
		this.bulkMetaDataExtension=JobPropertyMapService.getJobProperty((String)jobParam.get("jobName"),"org.innovatis.inMigration.bulkMetaDataExtension");
		this.bulkArchiveMetaDataFtl=JobPropertyMapService.getJobProperty((String)jobParam.get("jobName"),"org.innovatis.inMigration.bulkArchiveMetaDataFtl");
		this.exportDataPath=JobPropertyMapService.getJobProperty((String)jobParam.get("jobName"),"org.innovatis.inMigration.customArciveObjectMap.exportDataPath");
	}
	
	@Override
	public void write(List<? extends Map<String, String>> items) throws Exception {
		setProperties(jobParam);
		for (Map<String,String> bulkMetadata : items) {
			String fileCreationPath = jobParam.get("bulkDataPath") + bulkMetadata.get(folderID)+"//";

			File file = new File(fileCreationPath + bulkMetadata.get(fileName)
					+ bulkMetaDataExtension);
			if (!file.exists()) {
				file.createNewFile();
				log.debug("File is created %s", file.getAbsolutePath());
			} 

			// Write Content
			FileWriter writer = new FileWriter(file);
			bulkMetadata.put("type", "content");
			FreemarkerManager.get().process(bulkArchiveMetaDataFtl, bulkMetadata, writer);
			writer.close();
			File file1 = new File(fileCreationPath + bulkMetadata.get(fileName));
			File file2 = new File(exportDataPath+bulkMetadata.get(relativePath));
			bulkItemWriterHelper.copyFile(file2, file1);
		}
	}

}
