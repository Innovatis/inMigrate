package org.innovatis.inMigrate.freeMarkerManager;

import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.ObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import jersey.repackaged.com.google.common.collect.Maps;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;

/**
 * Manager enabling the View. Pulls in all resources needed for transforming the
 * freemarker templates.
 */
public class FreemarkerManager {
	private Configuration conf;
	private static FreemarkerManager instance = new FreemarkerManager();

	public static FreemarkerManager get() {
		return instance;
	}

	public FreemarkerManager() {
		conf = new Configuration();
		conf.setObjectWrapper(ObjectWrapper.DEFAULT_WRAPPER);
		conf.setTemplateLoader(new ClassTemplateLoader(this.getClass(), "/"));
	}

	public Map<String, ?> enhanceParams(Map<String, ?> input) {
		Map<String, ?> map = Maps.newLinkedHashMap(input);
		// Don't know yet if would need authentication
		return map;
	}

	public String process(String templateName, Map<String, ?> params) throws IOException, TemplateException {
		Writer sbw = new StringWriter();
		process(templateName, params, sbw);
		return sbw.toString();
	}

	public void process(String templateName, Map<String, ?> params, Writer out)
			throws IOException, TemplateException {
		Template template;
		template = conf.getTemplate(templateName);
		template.process(enhanceParams(params), out);
		out.close();
	}

}
