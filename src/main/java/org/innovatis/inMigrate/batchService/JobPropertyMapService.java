package org.innovatis.inMigrate.batchService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.innovatis.inMigrate.data.PropertyMap;
import org.springframework.stereotype.Component;

@Component
public class JobPropertyMapService {

	private static Map<String, Map<String,String>> jobPropertyMap =  new HashMap<>();
	
	public static void addJobProperty(String jobName, List<PropertyMap> propertyMapList){
		Map<String,String> jobProperty =  new HashMap<>();
		for(PropertyMap propertyMap: propertyMapList){
			jobProperty.put(propertyMap.getPropertyKey(),propertyMap.getPropertyValue());
		}
		jobPropertyMap.put(jobName, jobProperty);
	}
	
	public static Map<String,String> getJobProperties(String jobName){
		return jobPropertyMap.get(jobName);
	}
	
	public static String getJobProperty(String jobName, String propertyKey){
		if(jobPropertyMap.get(jobName)==null){
			return "";
		}
		return jobPropertyMap.get(jobName).get(propertyKey);
	}
}
