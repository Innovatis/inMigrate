package org.innovatis.inMigrate.batchService;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import org.innovatis.inMigrate.freeMarkerManager.FreemarkerManager;
import org.springframework.stereotype.Component;

import freemarker.template.TemplateException;

@Component
public class BulkItemProcessorHelper {

	
	public void fileDirectoryCreation(String bulkDataPath, String folderID, String fileName, String createdDate, String bulkItemMetaDataFtl, String bulkMetaDataExtension) throws IOException, TemplateException{
		File file1 = new File(bulkDataPath + folderID);
		File file2 = new File(bulkDataPath + folderID+bulkMetaDataExtension);
		if(!file2.exists() || !file1.exists()){
			if(!file1.exists()){
				file1.mkdir();
			}
			if(!file2.exists()){
				file2.createNewFile();
			}
			Map<String, String> map = new HashMap<String, String>();
			FileWriter writer = new FileWriter(file2);
			map.put(fileName, folderID);
			map.put(createdDate, LocalDateTime.now().toString());
			map.put("type", "folder");
			Map<String, HashMap<String,String>> tempMap = new HashMap<>();
			tempMap.put("bulkMetadata", (HashMap<String, String>) map);
			FreemarkerManager.get().process(bulkItemMetaDataFtl, tempMap, writer);
			writer.close();
		}
	}
	
	public Map<String, String> processMetadata(Map<String, String> item, boolean processMetadata, String requiredMetadata) {
		if(processMetadata){
			Map<String, String> map = new HashMap<String, String>();
			String[] requiredMetadataList = requiredMetadata.split(",");
			for(String tempMetadata : requiredMetadataList){
				if(item.get(tempMetadata)==null){
					map.put(tempMetadata, "nullValue");
				}else{
					map.put(tempMetadata, item.get(tempMetadata));
				}
			}
			return map;
		}
		return item;
	}
	
}
