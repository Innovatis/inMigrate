package org.innovatis.inMigrate.batchService;

import java.io.IOException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import freemarker.template.TemplateException;

@Component
public class CountFolderCreation implements FolderCreationStratergy{

	private Integer count = 0;
	
	private Integer folderIDCount = 1;
	
	private String folderID;
	
	private String fileName;
	
	private String createdDate;

	private String bulkMetaDataExtension;
	
	@Autowired
	BulkItemProcessorHelper bulkItemProcessorHelper;

	private void setProperties(String jobName) {
		this.bulkMetaDataExtension=JobPropertyMapService.getJobProperty(jobName,"org.innovatis.inMigration.bulkMetaDataExtension");
		this.folderID=JobPropertyMapService.getJobProperty(jobName,"org.innovatis.inMigration.customArciveObjectMap.folderID");
		this.fileName=JobPropertyMapService.getJobProperty(jobName,"org.innovatis.inMigration.customArciveObjectMap.fileName");
		this.createdDate=JobPropertyMapService.getJobProperty(jobName,"org.innovatis.inMigration.customArciveObjectMap.createdDate");
	}
	@Override
	public Map<String,String> createFolder(Map<String, String> metadata, String bulkDataPath, String jobName, String bulkArchiveMetaDataFtl) throws IOException, TemplateException {
		setProperties(jobName);
		if(count>1000){
			folderIDCount++;
			count=0;
		}
		metadata.put(folderID, folderIDCount.toString());
		bulkItemProcessorHelper.fileDirectoryCreation(bulkDataPath, metadata.get(folderID), fileName, createdDate, bulkArchiveMetaDataFtl, bulkMetaDataExtension);
		count++;
		return metadata;
	}

}
