package org.innovatis.inMigrate.batchService;

import java.io.File;
import java.time.LocalDateTime;
import java.util.List;

import org.innovatis.inMigrate.data.PropertyMap;
import org.innovatis.inMigrate.data.service.PropertyMapDataService;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

@Component
public class InboundEnpoint {
	
	@Autowired
	JobLauncher jobLauncher;
	
	@Autowired
	CSVDataStore CSVDataStore;
	
	@Autowired
	private PropertyMapDataService propertyMapDataService;
	
	@Autowired
	private ApplicationContext context;
	
	@Autowired
	BulkUploadService bulkUploadService;

	@ServiceActivator
	public String executeDBAlfrescoJob(Message<Integer> message) throws Exception {
		Integer retryCount =  1;
		if(message!=null&&message.getPayload()!=null){
			retryCount = message.getPayload() + 1;
		}
		String jobName = "DBAlfresco_"+LocalDateTime.now().toString().replace(":","");
		List<PropertyMap> propertyMapList = propertyMapDataService.findAll();
		JobPropertyMapService.addJobProperty(jobName, propertyMapList);
		String tempBulkDataPath = JobPropertyMapService.getJobProperty(jobName,"org.innovatis.inMigration.bulkDataPath") + jobName+"\\";
		File file = new File(tempBulkDataPath);
		if(!file.exists()){
			file.mkdir();
		}
		JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis())
				.addString("bulkDataPath", tempBulkDataPath).addString("jobName",jobName).addString("retryCount", retryCount.toString()).toJobParameters();
		Job job = (Job) context.getBean("dbFsAlfrescoJob");
		jobLauncher.run(job, jobParameters);
		return "job started";
	}
	
	@ServiceActivator
	public String executeUCMAlfrescoJob(Message<Integer> message) throws Exception {
		Integer retryCount =  1;
		if(message!=null&&message.getPayload()!=null){
			retryCount = message.getPayload() + 1;
		}
		String jobName = "UCMAlfresco_"+LocalDateTime.now().toString().replace(":","");
		List<PropertyMap> propertyMapList = propertyMapDataService.findAll();
		JobPropertyMapService.addJobProperty(jobName, propertyMapList);
		String tempBulkDataPath = JobPropertyMapService.getJobProperty(jobName, "org.innovatis.inMigration.bulkDataPath") + jobName+"\\";
		File file = new File(tempBulkDataPath);
		CSVDataStore.loadDataStore(JobPropertyMapService.getJobProperty(jobName, "org.innovatis.inMigration.customArciveObjectMap.CSVDataStorePath"),JobPropertyMapService.getJobProperty(jobName, "org.innovatis.inMigration.archiveExport.encoding"),JobPropertyMapService.getJobProperty(jobName, "org.innovatis.inMigration.customArciveObjectMap.CSVDataStoreKey"));
		if(!file.exists()){
			file.mkdir();
		}
		JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis())
				.addString("bulkDataPath", tempBulkDataPath).addString("jobName",jobName).addString("retryCount", retryCount.toString()).toJobParameters();
		Job job = (Job) context.getBean("ucmAlfrescoJob");
		jobLauncher.run(job, jobParameters);
		return "job started";
	}
	
	@ServiceActivator
	public String executeAlfrescoJob(Message<Integer> message) throws Exception {
		Integer retryCount =  1;
		if(message!=null&&message.getPayload()!=null){
			retryCount = message.getPayload() + 1;
		}
		String jobName = "Alfresco_"+LocalDateTime.now().toString().replace(":","");
		List<PropertyMap> propertyMapList = propertyMapDataService.findAll();
		JobPropertyMapService.addJobProperty(jobName, propertyMapList);
		String tempBulkDataPath = JobPropertyMapService.getJobProperty(jobName, "org.innovatis.inMigration.bulkDataPath") + jobName+"\\";
		File file = new File(tempBulkDataPath);
		//CSVDataStore.loadDataStore(JobPropertyMapService.getJobProperty(jobName, "org.innovatis.inMigration.customArciveObjectMap.CSVDataStorePath"),JobPropertyMapService.getJobProperty(jobName, "org.innovatis.inMigration.archiveExport.encoding"),JobPropertyMapService.getJobProperty(jobName, "org.innovatis.inMigration.customArciveObjectMap.CSVDataStoreKey"));
		if(!file.exists()){
			file.mkdir();
		}
		JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis())
				.addString("bulkDataPath", tempBulkDataPath).addString("jobName",jobName).addString("retryCount", retryCount.toString()).toJobParameters();
		Job job = (Job) context.getBean("alfrescoJob");
		jobLauncher.run(job, jobParameters);
		return "job started";
	}
}