package org.innovatis.inMigrate.batchService;

import java.nio.charset.Charset;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.batch.core.JobParameters;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Component
public class BulkUploadService {

	private String bulkDataPath;
	
	private String alfrescoTargetDirectory;
	
	private String alfrescoBulkUploadUrl;
	
	private String alfrescoUserName;
	
	private String alfrescoPassword;
	
	public void setProperties(JobParameters jobParam){
		this.bulkDataPath=jobParam.getString("bulkDataPath");
		this.alfrescoTargetDirectory=JobPropertyMapService.getJobProperty(jobParam.getString("jobName"),"org.innovatis.inMigration.alfrescoTargetDirectory");
		this.alfrescoBulkUploadUrl=JobPropertyMapService.getJobProperty(jobParam.getString("jobName"),"org.innovatis.inMigration.alfrescoBulkUploadUrl");
		this.alfrescoUserName=JobPropertyMapService.getJobProperty(jobParam.getString("jobName"),"org.innovatis.inMigration.alfrescoUserName");
		this.alfrescoPassword=JobPropertyMapService.getJobProperty(jobParam.getString("jobName"),"org.innovatis.inMigration.alfrescoPassword");
		
		}
	
	
	@ServiceActivator
	public void alfrescoBulkUpload(Message<JobParameters> message) {

		JobParameters jobParam = message.getPayload();
		setProperties(jobParam);
		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();
		parts.add("sourceDirectory", bulkDataPath);
		parts.add("targetPath", alfrescoTargetDirectory);
		parts.add("existingFileMode", "REPLACE");
		parts.add("submit", "Initiate Bulk Import");

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		String auth = alfrescoUserName + ":" + alfrescoPassword;
		byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("UTF-8")));
		String authHeader = "Basic " + new String(encodedAuth);
		headers.set("AUTHORIZATION", authHeader);
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<MultiValueMap<String, Object>>(parts,
				headers);
		ResponseEntity<String> response = restTemplate.exchange(
				alfrescoBulkUploadUrl, HttpMethod.POST, requestEntity, String.class);

	}
}
