package org.innovatis.inMigrate.batchService;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import org.springframework.stereotype.Component;

@Component
public class BulkItemWriterHelper {

	public void copyFile(File sourceFile, File destFile) throws FileNotFoundException, IOException {
		if (!sourceFile.exists()) {
			throw new FileNotFoundException("no file found" + sourceFile.getPath() +"while preparing data fo bulk file upload");
		}
		if (!destFile.exists()) {
			destFile.createNewFile();
		}
		FileChannel source = null;
		FileChannel destination = null;
		source = new FileInputStream(sourceFile).getChannel();
		destination = new FileOutputStream(destFile).getChannel();
		if (destination != null && source != null) {
			destination.transferFrom(source, 0, source.size());
		}
		if (source != null) {
			source.close();
		}
		if (destination != null) {
			destination.close();
		}

	}
}
