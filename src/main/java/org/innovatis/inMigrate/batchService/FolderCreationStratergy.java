package org.innovatis.inMigrate.batchService;

import java.io.IOException;
import java.util.Map;

import org.springframework.stereotype.Component;

import freemarker.template.TemplateException;

@Component
public interface FolderCreationStratergy {
	
	Map<String,String> createFolder(Map<String,String> metadata, String bulkDataPath, String jobName, String bulkArchiveMetaDataFtl) throws IOException, TemplateException;
}
