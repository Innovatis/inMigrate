package org.innovatis.inMigrate.batchService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

@Component
public class JobStatusService {

	private static final Logger log = LoggerFactory.getLogger(JobStatusService.class);

	@ServiceActivator
	public void saveStatus(Message<StepExecution> message){
		StepExecution stepExecution = message.getPayload();
		log.info("commitCount"+stepExecution.getCommitCount());
		log.info("ProcessSkipCount"+stepExecution.getProcessSkipCount());
		log.info("ReadCount"+stepExecution.getReadCount());
		log.info("ReadSkipCount"+stepExecution.getReadSkipCount());
		log.info("RollbackCount"+stepExecution.getRollbackCount());
		log.info("Summary"+stepExecution.getSummary());
		log.info("WriteCount"+stepExecution.getWriteCount());
		log.info("WriteSkipCount"+stepExecution.getWriteSkipCount());
		log.info("StepName"+stepExecution.getStepName());
		log.info("EndTime"+stepExecution.getEndTime());
		log.info("StartTime"+stepExecution.getStartTime());
		
	}
}
