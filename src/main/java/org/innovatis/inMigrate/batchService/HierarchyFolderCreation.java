package org.innovatis.inMigrate.batchService;

import java.io.IOException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import freemarker.template.TemplateException;

@Component
public class HierarchyFolderCreation implements FolderCreationStratergy{

	private String bulkMetaDataExtension;
	
	private String folderID;
	
	private String fileName;
	
	private String createdDate;
	
	private String folderCreationHierarchy;
	
	@Autowired
	BulkItemProcessorHelper bulkItemProcessorHelper;

	private void setProperties(String jobName) {
		this.bulkMetaDataExtension=JobPropertyMapService.getJobProperty(jobName,"org.innovatis.inMigration.bulkMetaDataExtension");
		this.folderID=JobPropertyMapService.getJobProperty(jobName,"org.innovatis.inMigration.customArciveObjectMap.folderID");
		this.fileName=JobPropertyMapService.getJobProperty(jobName,"org.innovatis.inMigration.customArciveObjectMap.fileName");
		this.createdDate=JobPropertyMapService.getJobProperty(jobName,"org.innovatis.inMigration.customArciveObjectMap.createdDate");
		this.folderCreationHierarchy=JobPropertyMapService.getJobProperty(jobName,"org.innovatis.inMigration.process.folderCreationHierarchy");
	}
	@Override
	public Map<String, String> createFolder(Map<String, String> metadata, String bulkDataPath, String jobName, String bulkArchiveMetaDataFtl)
			throws IOException, TemplateException {
		setProperties(jobName);
		String[] folderList = folderCreationHierarchy.split(",");
		String hierarcyFolder = "";
		for(String folderName : folderList){
			if(!metadata.get(folderName).equals(null)){
				folderName = metadata.get(folderName);
			}
			if(hierarcyFolder.equals("")){
				hierarcyFolder = folderName;
			} else {
				hierarcyFolder = hierarcyFolder + "\\" + folderName;
			}
			bulkItemProcessorHelper.fileDirectoryCreation(bulkDataPath, hierarcyFolder, fileName, createdDate, bulkArchiveMetaDataFtl, bulkMetaDataExtension);
		}
		metadata.put(folderID, hierarcyFolder);
		return metadata;
	}

	
}
