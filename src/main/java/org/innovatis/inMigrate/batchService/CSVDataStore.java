package org.innovatis.inMigrate.batchService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.innovatis.inMigrate.batchReader.CustomCSVWCCReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CSVDataStore {
	
	private static Map<String, Map<String,String>> data = new HashMap<>();
	
	@Autowired
	CustomCSVWCCReader reader;
	
	public void loadDataStore(String CSV_FILE_PATH, String encoding, String CSV_KEY) throws Exception {
		List<Map<String, String>> csvMapList = reader.doRead(CSV_FILE_PATH, encoding);
		for(Map<String,String> csvMap : csvMapList){
			data.put(csvMap.get(CSV_KEY), csvMap);
		}
	}
	
	public Map<String,String> getCSVRow(String rowKey) {
		return this.data.get(rowKey);
	}

	public String getCSVMapItem(String rowKey, String itemKey) {
		return this.data.get(rowKey).get(itemKey);
	}

	
}
