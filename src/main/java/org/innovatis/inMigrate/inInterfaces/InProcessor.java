package org.innovatis.inMigrate.inInterfaces;

import java.util.Map;
import org.springframework.batch.item.ItemProcessor;

public interface InProcessor extends ItemProcessor<Map<String,String>, Map<String,String>>{

}
