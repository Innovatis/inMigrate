package org.innovatis.inMigrate.inInterfaces;

import java.util.Map;
import org.springframework.batch.item.ItemWriter;

public interface InWriter extends ItemWriter<Map<String,String>>{

}
