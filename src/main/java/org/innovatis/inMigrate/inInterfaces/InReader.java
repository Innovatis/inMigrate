package org.innovatis.inMigrate.inInterfaces;

import java.util.Map;

import org.springframework.batch.item.ItemReader;

public interface InReader extends ItemReader<Map<String,String>>{

}
