package org.innovatis.inMigrate.batchListener;

import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.core.MessagingTemplate;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class CustomStepListener implements StepExecutionListener {

	@Autowired
	@Qualifier("statusChannel")
	MessageChannel statusChannel;
	
	@Override
	public void beforeStep(StepExecution stepExecution) {
		log.debug("StepExecutionListener - beforeStep");
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		
		if(stepExecution.getStatus() == BatchStatus.COMPLETED){
			log.debug("StepExecutionListener - afterStep");
			MessagingTemplate template = new MessagingTemplate();
			template.convertAndSend(statusChannel, stepExecution);
		}
		return stepExecution.getExitStatus();
	}

}