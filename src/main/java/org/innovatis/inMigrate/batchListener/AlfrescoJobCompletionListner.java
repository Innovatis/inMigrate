package org.innovatis.inMigrate.batchListener;

import org.innovatis.inMigrate.batchService.JobPropertyMapService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.JobParameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.core.MessagingTemplate;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Component;

@Component
public class AlfrescoJobCompletionListner implements JobExecutionListener {

	private static final Logger log = LoggerFactory.getLogger(AlfrescoJobCompletionListner.class);

	Integer maxRetryCount;
	
	@Autowired
	@Qualifier("requestChannel")
	MessageChannel requestChannel;
	
	@Autowired
	@Qualifier("reStartUCMAlfrescoChannel")
	MessageChannel reStartUCMAlfrescoChannel;
	
	@Autowired
	@Qualifier("reStartDBAlfrescoChannel")
	MessageChannel reStartDBAlfrescoChannel;

	public void setProperties(JobParameters jobParam){
		this.maxRetryCount=Integer.parseInt(JobPropertyMapService.getJobProperty(jobParam.getString("jobName"),"org.innovatis.inMigration.job.maxretrycount"));
		}
	
	@Override
	public void afterJob(JobExecution jobExecution) {
		setProperties(jobExecution.getJobParameters());
		if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
			log.info("!!! JOB FINISHED! Time to verify the results");
			MessagingTemplate template = new MessagingTemplate();
			template.convertAndSend(requestChannel, jobExecution.getJobParameters());
		}else if(jobExecution.getStatus() == BatchStatus.FAILED){
			log.info("!!! JOB Failed! restarting...");
			MessagingTemplate template = new MessagingTemplate();
			Integer retryCount = Integer.parseInt(jobExecution.getJobParameters().getString("retryCount"));
			if(jobExecution.getJobParameters().getString("jobName").startsWith("UCM")&&retryCount<maxRetryCount){
				template.convertAndSend(reStartUCMAlfrescoChannel, retryCount);
			}else if(jobExecution.getJobParameters().getString("jobName").startsWith("DB")&&retryCount<maxRetryCount){
				template.convertAndSend(reStartDBAlfrescoChannel, retryCount);
			}
		}
	}

	@Override
	public void beforeJob(JobExecution jobExecution) {
		// TODO Auto-generated method stub
		
	}
}