package org.innovatis.inMigrate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = { "org.innovatis.inMigrate" })
public class InMigrateApplication {

	public static void main(String[] args) {
		SpringApplication.run(InMigrateApplication.class, args);
	}
}