package org.innovatis.inMigrate.config;

import java.sql.SQLException;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver;
import org.springframework.orm.hibernate5.HibernateExceptionTranslator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "org.innovatis.inMigrate")
@PropertySource(value = { "classpath:application.properties" })

public class DbConfig {
	
	@Value("${init-db:false}")
	private String initDatabase;

	@Value("${db.driver}")
	private String alfrescoDbDriver;
	
	@Value("${db.username}")
	private String alfrescoDbUserName;
	
	@Value("${db.password}")
	private String alfrescoDbPassword;
	
	@Value("${db.url}")
	private String alfrescoDbUrl;
	
	@Value("${jdbc.username}")
	private String user;

	@Value("${jdbc.driverClassName}")
	private String driverClassName;

	@Value("${jdbc.url}")
	private String url;

	@Value("${hibernate.hbm2ddl.auto}")
	private String hiberateAuto;

	@Value("${hibernate.dialect}")
	private String hibernateDialect;

	@Bean
	public static PropertySourcesPlaceholderConfigurer placeHolderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	@Bean
	public PlatformTransactionManager transactionManager() throws SQLException {
		EntityManagerFactory factory = entityManagerFactory().getObject();
		return new JpaTransactionManager(factory);
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws SQLException {
		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setGenerateDdl(Boolean.TRUE);
		vendorAdapter.setShowSql(Boolean.TRUE);
		factory.setDataSource(dataSource());
		factory.setJpaVendorAdapter(vendorAdapter);
		factory.setPackagesToScan("org.innovatis.inMigrate");
		Properties jpaProperties = new Properties();
		jpaProperties.put("hibernate.hbm2ddl.auto", hiberateAuto);
		jpaProperties.put("hibernate.dialect", hibernateDialect);
		factory.setJpaProperties(jpaProperties);
		factory.afterPropertiesSet();
		factory.setLoadTimeWeaver(new InstrumentationLoadTimeWeaver());
		return factory;
	}

	@Bean
	public HibernateExceptionTranslator hibernateExceptionTranslator() {
		return new HibernateExceptionTranslator();
	}

	@Bean
	@Primary
	public DataSource dataSource() throws SQLException {
		return DataSourceBuilder.create().username(user).url(url).driverClassName(driverClassName)
				.build();
	}
	

	@Bean
	public DataSource alfrescoDataSource() throws SQLException {
		return DataSourceBuilder.create().username(alfrescoDbUserName).password(alfrescoDbPassword).url(alfrescoDbUrl).driverClassName(alfrescoDbDriver)
				.build();
	}
}
