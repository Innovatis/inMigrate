package org.innovatis.inMigrate.config;

import org.innovatis.inMigrate.batchService.BulkUploadService;
import org.innovatis.inMigrate.batchService.InboundEnpoint;
import org.innovatis.inMigrate.batchService.JobStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Description;
import org.springframework.http.HttpMethod;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegrationManagement;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.http.dsl.Http;
import org.springframework.messaging.MessageChannel;

@Configuration
@IntegrationComponentScan( "org.innovatis.inMigrate")
@EnableIntegrationManagement
public class IntegrationConfig {
	
	@Autowired
	InboundEnpoint inboundEnpoint;
	

	@Autowired
	BulkUploadService bulkUploadService;
	
	@Autowired
	JobStatusService jobStatusService;
	
	@Bean
	@Description("Entry to the messaging system through the gateway.")
	public MessageChannel requestChannel() {
		return new DirectChannel();
	}
	
	@Bean
	@Description("Entry to the messaging system through the gateway.")
	public MessageChannel reStartUCMAlfrescoChannel() {
		return new DirectChannel();
	}
	
	@Bean
	@Description("Entry to the messaging system through the gateway.")
	public MessageChannel reStartDBAlfrescoChannel() {
		return new DirectChannel();
	}
	
	@Bean
	@Description("Entry to the messaging system through the gateway.")
	public MessageChannel statusChannel() {
		return new DirectChannel();
	}

	@Bean
	IntegrationFlow integrationFlowDB(){
		return IntegrationFlows.from(Http.inboundGateway("/DBJob").requestMapping(m->m.methods(HttpMethod.POST))).handle(inboundEnpoint,"executeDBAlfrescoJob").get();
	}
	
	@Bean
	IntegrationFlow integrationFlowUCM(){
		return IntegrationFlows.from(Http.inboundGateway("/UCMJob").requestMapping(m->m.methods(HttpMethod.POST))).handle(inboundEnpoint,"executeUCMAlfrescoJob").get();
	}
	
	@Bean
	IntegrationFlow integrationFlowAlf(){
		return IntegrationFlows.from(Http.inboundGateway("/AlfJob").requestMapping(m->m.methods(HttpMethod.POST))).handle(inboundEnpoint,"executeAlfrescoJob").get();
	}
	
	@Bean
	IntegrationFlow integrationFlowUpload(){
		return IntegrationFlows.from("requestChannel").handle(bulkUploadService,"alfrescoBulkUpload").get();
	}

	@Bean
	IntegrationFlow integrationFlowUCMAlfrescoRestart(){
		return IntegrationFlows.from("reStartUCMAlfrescoChannel").handle(inboundEnpoint,"executeUCMAlfrescoJob").get();
	}
	

	@Bean
	IntegrationFlow integrationFlowDBAlfrescoRestart(){
		return IntegrationFlows.from("reStartDBAlfrescoChannel").handle(inboundEnpoint,"executeDBAlfrescoJob").get();
	}
	
	@Bean
	IntegrationFlow integrationFlowSatus(){
		return IntegrationFlows.from("statusChannel").handle(jobStatusService,"saveStatus").get();
	}
	
}
