package org.innovatis.inMigrate.config;

import java.io.FileNotFoundException;
import java.util.Map;

import org.innovatis.inMigrate.batchListener.CustomStepListener;
import org.innovatis.inMigrate.inInterfaces.InProcessor;
import org.innovatis.inMigrate.inInterfaces.InReader;
import org.innovatis.inMigrate.inInterfaces.InWriter;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DbFsAlfrescoStepConfig {

	@Autowired
	@Qualifier("dbReader")
	private InReader dbReader;
	
	@Autowired
	@Qualifier("dbFsAlfrescoProcessor")
	private InProcessor dbFsAlfrescoProcessor;
	
	@Autowired
	@Qualifier("dbFsAlfrescoWriter")
	private InWriter dbFsAlfrescoWriter;
	
	@Autowired
	public StepBuilderFactory stepBuilderFactory;

	@Autowired
	public CustomStepListener customStepListener;
	
	@Bean
	public Step dbFsAlfrescoStep() {
		return stepBuilderFactory.get("dbFsAlfrescoStep").<Map<String,String>, Map<String,String>>chunk(1).reader(dbReader)
				.processor(dbFsAlfrescoProcessor).writer(dbFsAlfrescoWriter).faultTolerant()
			       .skip(FileNotFoundException.class)
			       .skipLimit(100000).listener(customStepListener).build();
	}
	
}
