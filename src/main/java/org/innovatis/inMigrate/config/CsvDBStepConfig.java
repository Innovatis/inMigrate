package org.innovatis.inMigrate.config;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.innovatis.inMigrate.batchListener.CustomStepListener;
import org.innovatis.inMigrate.batchProcessor.PersonItemProcessor;
import org.innovatis.inMigrate.batchReader.PersonFieldSetMapper;
import org.innovatis.inMigrate.data.Person;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

@Configuration
public class CsvDBStepConfig {

	@Autowired
	@Qualifier("dataSource")
	public DataSource dataSource;
	
	@Autowired
	@Qualifier("alfrescoDataSource")
	public DataSource alfrescoDataSource;

	@Autowired
	public StepBuilderFactory stepBuilderFactory;

	@Autowired
	public CustomStepListener customStepListener;
	@Bean
	public FlatFileItemReader<Person> reader() {
		FlatFileItemReader<Person> reader = new FlatFileItemReader<Person>();
		reader.setResource(new ClassPathResource("sample-data.csv"));
		reader.setLineMapper(new DefaultLineMapper<Person>() {
			{
				setLineTokenizer(new DelimitedLineTokenizer() {
					{
						setNames(new String[] { "firstName", "lastName" });
					}
				});
				setFieldSetMapper(new PersonFieldSetMapper());
			}
		});
		return reader;
	}
	
	@Bean
	public PersonItemProcessor processor() {
		return new PersonItemProcessor();
	}
	
	@Bean
	public JdbcBatchItemWriter<Person> writer() throws SQLException {
		JdbcBatchItemWriter<Person> writer = new JdbcBatchItemWriter<Person>();
		writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<Person>());
		writer.setSql("INSERT INTO people (id,firstName, lastName) VALUES (PEOPLE_SEQ.nextval,:firstName, :lastName)");
		writer.setDataSource(dataSource);
		return writer;
	}

	@Bean
	public Step csvDbStep() throws SQLException {
		return stepBuilderFactory.get("csvDbStep").<Person, Person>chunk(10).reader(reader()).processor(processor())
				.writer(writer()).faultTolerant()
			       .skip(Exception.class)
			       .skipLimit(100000).listener(customStepListener)
			       .build();
	}
}
