package org.innovatis.inMigrate.config;

import java.util.Map;

import javax.sql.DataSource;

import org.innovatis.inMigrate.batchProcessor.AlfrescoProcessor;
import org.innovatis.inMigrate.batchProcessor.DbFsAlfrescoProcessorImpl;
import org.innovatis.inMigrate.batchProcessor.UcmAlfrescoProcessorImpl;
import org.innovatis.inMigrate.batchReader.AlfrescoReader;
import org.innovatis.inMigrate.batchReader.DbReaderImpl;
import org.innovatis.inMigrate.batchReader.HdaUcmReader;
import org.innovatis.inMigrate.batchWriter.AlfrescoWriter;
import org.innovatis.inMigrate.batchWriter.DbFsAlfrescoWriterImpl;
import org.innovatis.inMigrate.batchWriter.UcmAlfrescoWriterImpl;
import org.innovatis.inMigrate.transformer.AlfrescoDataTransformer;
import org.innovatis.inMigrate.transformer.impl.AlfrescoDataTransformerImpl;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

@Configuration
public class JobBeansConfig {

	@Autowired
	@Qualifier("dataSource")
	private DataSource dataSource;
	

	@Autowired
	@Qualifier("alfrescoDataSource")
	public DataSource alfrescoDataSource;
	
	private static Map testMap = null;
	
	@Bean
	@JobScope
	public Map jobParam(@Value("#{jobParameters}")Map jobParam){
		return jobParam;
	}
	
	@Bean
	@ConditionalOnMissingBean(name="ucmReader")
	public HdaUcmReader ucmReader() {
		HdaUcmReader reader = new HdaUcmReader();
		reader.setJobParam(jobParam(testMap));
		return reader;
	}
	
	@Bean
	@ConditionalOnMissingBean(name="dbReader")
	public DbReaderImpl dbReader() {
		DbReaderImpl databaseReader = new DbReaderImpl();
		databaseReader.setJobParam(jobParam(testMap));
		databaseReader.setDataSource(dataSource);
		databaseReader.setRowMapper(new BeanPropertyRowMapper<>());
		return databaseReader;
	}
	
	@Bean
	@ConditionalOnMissingBean(name="ucmAlfrescoProcessor")
	public UcmAlfrescoProcessorImpl ucmAlfrescoProcessor() {
		UcmAlfrescoProcessorImpl ucmAlfrescoProcessorImpl = new UcmAlfrescoProcessorImpl();
		ucmAlfrescoProcessorImpl.setJobParam(jobParam(testMap));
		return ucmAlfrescoProcessorImpl;
	}
	
	@Bean
	@ConditionalOnMissingBean(name="dbFsAlfrescoProcessor")
	public DbFsAlfrescoProcessorImpl dbFsAlfrescoProcessor() {
		DbFsAlfrescoProcessorImpl bulkProcessor = new DbFsAlfrescoProcessorImpl();
		bulkProcessor.setJobParam(jobParam(testMap));
		return bulkProcessor;
	}
	
	@Bean
	@ConditionalOnMissingBean(name="dbFsAlfrescoWriter")
	public DbFsAlfrescoWriterImpl dbFsAlfrescoWriter() {
		DbFsAlfrescoWriterImpl bulkDataItemWriter = new DbFsAlfrescoWriterImpl();
		bulkDataItemWriter.setJobParam(jobParam(testMap));
		return bulkDataItemWriter;
	}
	
	@Bean
	@ConditionalOnMissingBean(name="ucmAlfrescoWriter")
	public UcmAlfrescoWriterImpl ucmAlfrescoWriter() {
		UcmAlfrescoWriterImpl ucmAlfrescoWriterImpl = new UcmAlfrescoWriterImpl();
		ucmAlfrescoWriterImpl.setJobParam(jobParam(testMap));
		return ucmAlfrescoWriterImpl;
	}
	
	@Bean
	@ConditionalOnMissingBean(name="alfrescoWriter")
	public AlfrescoWriter alfrescoWriter() {
		AlfrescoWriter alfrescoWriter = new AlfrescoWriter();
		alfrescoWriter.setJobParam(jobParam(testMap));
		return alfrescoWriter;
	}
	
	@Bean
	@ConditionalOnMissingBean(name="alfrescoProcessor")
	public AlfrescoProcessor alfrescoProcessor() {
		AlfrescoProcessor alfrescoProcessor = new AlfrescoProcessor();
		alfrescoProcessor.setJobParam(jobParam(testMap));
		return alfrescoProcessor;
	}
	
	@Bean
	@ConditionalOnMissingBean(name="alfrescoReader")
	public AlfrescoReader alfrescoReader() {
		AlfrescoReader reader = new AlfrescoReader();
		reader.setDataSource(alfrescoDataSource);
		reader.setRowMapper(new BeanPropertyRowMapper<>());
		reader.setJobParam(jobParam(testMap));
		return reader;
	}
	
	@Bean
	@ConditionalOnMissingBean(name="alfrescoDataTransformerImpl")
	public AlfrescoDataTransformerImpl alfrescoDataTransformerImpl() {
		AlfrescoDataTransformerImpl alfrescoProcessor = new AlfrescoDataTransformerImpl();
		return alfrescoProcessor;
	}
}
