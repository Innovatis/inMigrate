package org.innovatis.inMigrate.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.innovatis.inMigrate.batchListener.AlfrescoJobCompletionListner;
import org.innovatis.inMigrate.batchListener.JobCompletionNotificationListener;

@Configuration
@EnableBatchProcessing
public class BatchConfig {

	@Autowired
	public JobBuilderFactory jobBuilderFactory;

	@Autowired
	@Qualifier("csvDbStep")
	Step csvDbStep;
	
	@Autowired
	@Qualifier("ucmAlfrescoStep")
	Step ucmAlfrescoStep;
	
	@Autowired
	@Qualifier("alfrescoStep")
	Step alfrescoStep;
	
	@Autowired
	@Qualifier("dbFsAlfrescoStep")
	Step dbFsAlfrescoStep;
	
	@Bean
	public Job csvDbJob(JobCompletionNotificationListener listener) {
		return jobBuilderFactory.get("csvDbJob").incrementer(new RunIdIncrementer()).listener(listener)
				.flow(csvDbStep).end().build();
	}

	@Bean
	public Job dbFsAlfrescoJob(AlfrescoJobCompletionListner listener) {
		return jobBuilderFactory.get("dbFsAlfrescoJob").incrementer(new RunIdIncrementer()).listener(listener).flow(dbFsAlfrescoStep)
				.end().build();
	}

	@Bean
	public Job ucmAlfrescoJob(AlfrescoJobCompletionListner listener) {
		return jobBuilderFactory.get("ucmAlfrescoJob").incrementer(new RunIdIncrementer()).listener(listener).flow(ucmAlfrescoStep)
				.end().build();
	}
	
	@Bean
	public Job alfrescoJob(AlfrescoJobCompletionListner listener) {
		return jobBuilderFactory.get("alfrescoJob").incrementer(new RunIdIncrementer()).listener(listener).flow(alfrescoStep)
				.end().build();
	}
	
}
