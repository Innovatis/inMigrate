package org.innovatis.inMigrate.config;

import java.util.Map;

import org.innovatis.inMigrate.batchListener.CustomStepListener;
import org.innovatis.inMigrate.inInterfaces.InProcessor;
import org.innovatis.inMigrate.inInterfaces.InReader;
import org.innovatis.inMigrate.inInterfaces.InWriter;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UcmAlfrescoStepConfig {

	@Autowired
	@Qualifier("ucmReader")
	private InReader ucmReader;

	@Autowired
	@Qualifier("ucmAlfrescoProcessor")
	private InProcessor ucmAlfrescoProcessor;
	
	@Autowired
	@Qualifier("ucmAlfrescoWriter")
	private InWriter ucmAlfrescoWriter;

	@Autowired
	public StepBuilderFactory stepBuilderFactory;

	@Autowired
	public CustomStepListener customStepListener;
	

	@Bean
	public Step ucmAlfrescoStep() {
		return stepBuilderFactory.get("ucmAlfrescoStep").<Map<String,String>, Map<String,String>>chunk(1).reader(ucmReader).processor(ucmAlfrescoProcessor)
				.writer(ucmAlfrescoWriter).faultTolerant()
			       .skip(Exception.class)
			       .skipLimit(100000).listener(customStepListener)
			       .build();
	}
}
