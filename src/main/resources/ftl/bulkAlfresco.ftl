[#ftl]
<?xml version="1.0" encoding="UTF-8"?>
 <!DOCTYPE properties SYSTEM "http://java.sun.com/dtd/properties.dtd">
 <properties>
    
    <entry key="aspects">cm:versionable,cm:dublincore</entry>
    [#if bulkMetadata??]
        [#list bulkMetadata?keys as prop]
        	[#if prop=="type"]
    			<entry key="${prop}">cm:${bulkMetadata[prop]}</entry>
    	 	[#else]
	         [#if bulkMetadata[prop]?? && bulkMetadata[prop]?trim?has_content]
	            <entry key="cm:${prop}">${bulkMetadata[prop]}</entry>
	         [/#if]
         	[/#if]	
        [/#list]
    [/#if]
   
  </properties>