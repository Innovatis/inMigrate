[#ftl]
<?xml version="1.0" encoding="UTF-8"?>
 <!DOCTYPE properties SYSTEM "http://java.sun.com/dtd/properties.dtd">
 <properties>
     [#if type?? && type?trim?has_content]
    	<entry key="type">cm:${type}</entry>
    	[#if dDocCreatedDate?? && dDocCreatedDate?trim?has_content]
	    	[#if type=="folder"]
		    	<entry key="cm:created">${dDocCreatedDate}</entry>
		    	<entry key="cm:modified">${dDocCreatedDate}</entry>
		    [#else]
			    [#if type=="content"]
			    	[#assign aDateTime=dDocCreatedDate?substring(5, 28)?datetime("yyyy-MM-dd hh:mm:ss.SSS")]
			    	<entry key="cm:created">${aDateTime?string("yyyy-MM-dd")}T${aDateTime?string("HH:mm:ss.SSS")}</entry>
			    [/#if]
			[/#if]
		[/#if]
    [/#if]
    <entry key="aspects">cm:versionable,cm:dublincore</entry>
    <entry key="cm:title">A photo of a flower.</entry>
    [#if xComments?? && xComments?trim?has_content]
    	<entry key="cm:description">${xComments}</entry>
    [/#if]
    [#if dDocAuthor?? && dDocAuthor?trim?has_content]
    	<entry key="cm:author">${dDocAuthor}</entry>
     [/#if]
    <entry key="cm:publisher">Peter Monks</entry>
    <entry key="cm:contributor">Peter Monks</entry>
     [#if dWebExtension?? && dWebExtension?trim?has_content]
    	<entry key="cm:type">${dWebExtension}</entry>
     [/#if]
    <entry key="cm:identifier">IMG_1967.jpg</entry>
    [#if dDocLastModifiedDate?? && dDocLastModifiedDate?trim?has_content]
		[#assign aDateTime=dDocLastModifiedDate?substring(5, 28)?datetime("yyyy-MM-dd hh:mm:ss.SSS")]
		<entry key="cm:modified">${aDateTime?string("yyyy-MM-dd")}T${aDateTime?string("HH:mm:ss.SSS")}</entry>
	[/#if]
    <entry key="cm:dcsource">Canon Powershot G2</entry>
    <entry key="cm:coverage">Worldwide</entry>
    <entry key="cm:rights">Copyright (c) Peter Monks 2002, All Rights Reserved</entry>
    <entry key="cm:subject">A photo of a flower.</entry>
  </properties>