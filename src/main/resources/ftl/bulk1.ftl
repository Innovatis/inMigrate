[#ftl]
<?xml version="1.0" encoding="UTF-8"?>
 <!DOCTYPE properties SYSTEM "http://java.sun.com/dtd/properties.dtd">
 <properties>
     [#if file?? && file?trim?has_content]
    	<entry key="type">cm:${file}</entry>
    [/#if]
    <entry key="aspects">cm:versionable,cm:dublincore</entry>
    <entry key="cm:title">A photo of a flower.</entry>
    [#if bulkMetadata.description?? && bulkMetadata.description?trim?has_content]
    	<entry key="cm:description">${bulkMetadata.description}</entry>
    [/#if]
    [#if bulkMetadata.createdDate?? && bulkMetadata.createdDate?trim?has_content]
    	<entry key="cm:created">${bulkMetadata.createdDate}</entry>
    [/#if]
    <!-- cm:dublincore properties -->
    <entry key="cm:author">Peter Monks</entry>
    <entry key="cm:publisher">Peter Monks</entry>
    <entry key="cm:contributor">Peter Monks</entry>
     [#if bulkMetadata.type?? && bulkMetadata.type?trim?has_content]
    	<entry key="cm:type">${bulkMetadata.type}</entry>
     [/#if]
    <entry key="cm:identifier">IMG_1967.jpg</entry>
    [#if bulkMetadata.modifiedDate?? && bulkMetadata.modifiedDate?trim?has_content]
		<entry key="cm:modified">${bulkMetadata.modifiedDate}</entry>
	[/#if]
    <entry key="cm:dcsource">Canon Powershot G2</entry>
    <entry key="cm:coverage">Worldwide</entry>
    <entry key="cm:rights">Copyright (c) Peter Monks 2002, All Rights Reserved</entry>
    <entry key="cm:subject">A photo of a flower.</entry>
  </properties>