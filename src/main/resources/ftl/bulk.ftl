[#ftl]
<?xml version="1.0" encoding="UTF-8"?>
 <!DOCTYPE properties SYSTEM "http://java.sun.com/dtd/properties.dtd">
 <properties>
     [#if type?? && type?trim?has_content]
    	<entry key="type">cm:${type}</entry>
    	[#if CREATEDDATE?? && CREATEDDATE?trim?has_content]
	    	[#if type=="folder"]
		    	<entry key="cm:created">${CREATEDDATE}</entry>
		    	<entry key="cm:modified">${CREATEDDATE}</entry>
		    [#else]
			    [#if type=="content"]
			    	[#assign aDateTime=CREATEDDATE?datetime("yyyy-MM-dd hh:mm:ss.SSS")]
			    	<entry key="cm:created">${aDateTime?string("yyyy-MM-dd")}T${aDateTime?string("HH:mm:ss.SSS")}</entry>
			    [/#if]
			[/#if]
		[/#if]
    [/#if]
    <entry key="aspects">cm:versionable,cm:dublincore</entry>
    <entry key="cm:title">A photo of a flower.</entry>
    [#if DESCRIPTION?? && DESCRIPTION?trim?has_content]
    	<entry key="cm:description">${DESCRIPTION}</entry>
    [/#if]
    <!-- cm:dublincore properties -->
    <entry key="cm:author">Peter Monks</entry>
    <entry key="cm:publisher">Peter Monks</entry>
    <entry key="cm:contributor">Peter Monks</entry>
     [#if FILETYPE?? && FILETYPE?trim?has_content]
    	<entry key="cm:type">${FILETYPE}</entry>
     [/#if]
    <entry key="cm:identifier">IMG_1967.jpg</entry>
    [#if MODIFIEDDATE?? && MODIFIEDDATE?trim?has_content]
		[#assign aDateTime=MODIFIEDDATE?datetime("yyyy-MM-dd hh:mm:ss.SSS")]
		<entry key="cm:modified">${aDateTime?string("yyyy-MM-dd")}T${aDateTime?string("HH:mm:ss.SSS")}</entry>
	[/#if]
    <entry key="cm:dcsource">Canon Powershot G2</entry>
    <entry key="cm:coverage">Worldwide</entry>
    <entry key="cm:rights">Copyright (c) Peter Monks 2002, All Rights Reserved</entry>
    <entry key="cm:subject">A photo of a flower.</entry>
  </properties>