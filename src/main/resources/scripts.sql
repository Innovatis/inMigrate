
Alter table PROPERTYMAP ALTER Column PROPERTYVALUE nvarchar(1028);
INSERT INTO PROPERTYMAP (id,propertyKey, propertyValue ) VALUES
(1,'org.innovatis.inMigration.process.requiredMetadata', 'dDocCreatedDate,dDocLastModifiedDate,dDocAuthor,xComments,primaryFile,dOriginalName,dWebExtension'), (2,'org.innovatis.inMigration.processMetadata', 'true' ),(3,'org.innovatis.inMigration.customArciveObjectMap.CSVDataStoreKey','dOriginalName'),(4,'org.innovatis.inMigration.archiveExport.encoding','UTF-8');

INSERT INTO PROPERTYMAP (id, PROPERTYKEY,PROPERTYVALUE) VALUES (5,'org.innovatis.inMigration.archiveExport.encoding','UTF-8'),
(6,'org.innovatis.inMigration.customArciveObjectMap.exportDataPath','C:\\Users\\murali\\Downloads\\MountApp\\mdware\\user_projects\\domains\\WCCDomain\\ucm\\cs\\archives\\ramp\\'),
(7,'org.innovatis.inMigration.bulkArchiveMetaDataFtl','ftl/bulkArchive.ftl'),
(8,'org.innovatis.inMigration.bulkMetaDataExtension','.metadata.properties.xml'),
(9,'org.innovatis.inMigration.customArciveObjectMap.relativePath','primaryFile'),
(10,'org.innovatis.inMigration.customArciveObjectMap.fileName','dOriginalName'),
(11,'org.innovatis.inMigration.customArciveObjectMap.folderID','FOLDERID'),
(12,'org.innovatis.inMigration.bulkArchivedDataPath','C:\\Users\\murali\\Downloads\\MountApp\\Bulk\\'),
(13,'org.innovatis.inMigration.job.maxretrycount','3'),
(14,'org.innovatis.inMigration.customArciveObjectMap.CSVDataStorePath','C:\\Users\\murali\\Downloads\\MountApp\\Bulk\\Data.csv'),
(15,'org.innovatis.inMigration.customArciveObjectMap.exportDataPath','C:\\Users\\murali\\Downloads\\MountApp\\mdware\\user_projects\\domains\\WCCDomain\\ucm\\cs\\archives\\ramp\\'),
(16,'org.innovatis.inMigration.customArciveObjectMap.metadataFilePath','17-sep-25_14.25.22_233_01\\17268142522~1.hda'),
(17,'org.innovatis.inMigration.customArciveObjectMap.createdDate','dDocCreatedDate'),
(18,'org.innovatis.inMigration.process.folderCreationHierarchy','dWebExtension,dDocAuthor,dOriginalName'),
(19,'org.innovatis.inMigration.alfrescoTargetDirectory','/AlTransco'),
(20,'org.innovatis.inMigration.alfrescoBulkUploadUrl','http://localhost:8081/alfresco/s/bulkfsimport/initiate'),
(21,'org.innovatis.inMigration.alfrescoUserName','admin'),
(22,'org.innovatis.inMigration.alfrescoPassword','admin'),
(23,'org.innovatis.inMigration.customObjectMap.metadataSQL','SELECT id, name, path, createdDate, modifiedDate, type as FILETYPE, description, folderId from BULKFILES'),
(24,'org.innovatis.inMigration.bulkMetaDataFtl','ftl/bulk.ftl'),
(25,'org.innovatis.inMigration.customObjectMap.folderID','FOLDERID'),
(26,'org.innovatis.inMigration.customObjectMap.fileName','NAME'),
(27,'org.innovatis.inMigration.customObjectMap.createdDate','CREATEDDATE'),
(28, 'org.innovatis.inMigration.bulkDataPath','C:\\Users\\murali\\Downloads\\MountApp\\Bulk\\'),
(29, 'org.innovatis.inMigration.customObjectMap.path','PATH'),
(30,'org.innovatis.inMigration.alfresco.metadata.sql','select node_id, actual_type_n, persisted_type_n, boolean_value, long_value, float_value, double_value, string_value, qname_id, aq.local_name AS LOCAL_NAME from alf_node_properties anp, alf_qname aq where anp.qname_id = aq.id and node_id ='),
(31,'org.innovatis.inMigration.alfresco.file.sql','select an.id AS Node_Id, an.uuid AS UUID, content_url AS Content_URL, an.audit_creator AS Creator, an.audit_created AS Creation_Date, an.audit_modifier AS Modifier, an.audit_modified AS Modification_Date, am.mimetype_str AS Mimetype from alf_content_url acu, alf_content_data acd, alf_node_properties anp, alf_node an, alf_qname aq, alf_mimetype am where acd.content_url_id = acu.id and anp.long_value = acd.id and anp.node_id = an.id and an.type_qname_id = aq.id and acd.content_mimetype_id = am.id and aq.local_name=''post''');

INSERT INTO PROPERTYMAP (id, PROPERTYKEY,PROPERTYVALUE) VALUES (32,'org.innovatis.inMigration.alfresco.metadata.db.key','local_name'),
(33,'org.innovatis.inMigration.alfresco.metadata.db.value','string_value'),
(34,'org.innovatis.inMigration.alfresco.file.nodeId','node_id'),
(35,'org.innovatis.inMigration.alfrescoFtl','ftl/bulkAlfresco.ftl'),
(36,'org.innovatis.inMigration.alfresco.fileName','name'),
(37,'org.innovatis.inMigration.alfresco.exportDataPath','C:\\alfresco-community\\alf_data\\contentstore\\'),
(38,'org.innovatis.inMigration.alfresco.relativePath','content_url');

Insert into BULKFILES(id, NAME, path, createdDate, description, type, modifiedDate, folderId) values(2, '9.jpg', 'C:\\Users\\murali\\Downloads\\9.jpg', CURRENT_TIMESTAMP,'pic','jpg',CURRENT_TIMESTAMP, 1)
COMMIT
